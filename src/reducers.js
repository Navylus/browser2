import { combineReducers } from 'redux'

import news from './components/news/reducer'

const reducers = {
  news
}

export default combineReducers(reducers)
