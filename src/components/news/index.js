import React, { Component } from 'react'
import { connect } from 'react-redux'
import Results from './components/results'
import { getEventsData } from './actions'

class News extends Component {
  componentDidMount() {
    getEventsData()
  }

  render() {
    const { news } = this.props
    return (
      <div>
        <Results data={news.data} />
      </div>
    )
  }
}

export default connect(state => state)(News)
