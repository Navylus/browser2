import { fromJS } from 'immutable'
import initialState from './initial-state'
import actionType from '../actions/actions-type'
import '../actions'

const getLastEvents = (state, action) => (
  fromJS(state)
    .setIn(['data'], action.data)
    .toJS()
)
const news = (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_LAST_EVENTS:
      return getLastEvents(state, action)
    default:
      return state
  }
}

export default news
